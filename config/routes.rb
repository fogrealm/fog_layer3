Rails.application.routes.draw do

  root to: 'static_pages#home'

  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resource :static_pages, only: [] do
    collection do
      get :home
    end
  end

  resources :house_events do
    collection do
      post :converge_fog_nodes_events
    end
  end 

  resources :houses do
    collection do
      post :analyze_performance
    end
  end

end