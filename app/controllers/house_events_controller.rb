class HouseEventsController < InheritedResources::Base
  respond_to :json

  before_action :validate_payloaded_message, only: [ :converge_fog_nodes_events, :create ]


  def converge_fog_nodes_events

    puts "\n payload_data_hash received - #{@payload_data_hash}"
    house_id = @payload_data_hash["house_id"].to_i
    infra_parameter_id = @payload_data_hash["infra_parameter_id"].to_i
    measurement = @payload_data_hash["measurement"].to_f

    if house_id.present? && infra_parameter_id.present? && measurement.present?
      $redis.setex("cached_icfn_entry_#{house_id}_#{infra_parameter_id}_timestamp_#{Time.now.to_i}", 10 * 60, measurement)
    else
      render json: {
        status: 422,
        error: "Message from Interconnecting Fog Controller Instance Invalid Request from Fog"
      } and return
    end
  end


  def create
    puts "\n payload_data_hash received - #{@payload_data_hash}"
    house_event_hash = @payload_data_hash["house_event"]
    @house_event = HouseEvent.new(house_event_hash) rescue nil

    if @house_event.nil?
      render json: {
        status: 422,
        error: "Some issue occured while saving the house event in ICFN Node"
      } and return
    end

    create! do |format|
      if @house_event.errors.any?
        render json: {
          status: 422, 
          error: "Issue encountered while saving house event in ICNF Node"
        } and return
      else
        render json: {
          status: 204,
          message: "House event saved in ICFN Node"
        } and return
      end
    end
  end  


  private

  def validate_payloaded_message

    @requested_house = House.where(id: params[:house_id].to_i).first
    @payload_data_hash = FogSecurityManager.payload_data_hash_from_symmetrically_encrypted_payload_data(params[:encrypted_payload], @requested_house.icfn_channel_key, nil, nil)# rescue nil

    if @requested_house.nil? || @payload_data_hash.nil?
      raise CanCan::AccessDenied
    end

    received_message_digest = params[:message_digest]
    
    if @payload_data_hash.present?
      computed_message_digest = OpenSSL::HMAC.hexdigest("SHA256", @requested_house.icfn_channel_key, @payload_data_hash.to_json)
    
      if computed_message_digest != received_message_digest
        raise CanCan::AccessDenied
      end
    end
  end


  def permitted_params
    params.permit(house_event: [:id, :name, :event_level, :house_event_type, :house_id, :house_section_name, :event_message, :reported_at_timestamp])
  end

end