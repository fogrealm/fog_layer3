class IcfnMain
  include Actions::IcfnMain

  # => Constants ...
  CREDENTIALS_CONFIG = YAML.load_file("#{::Rails.root}/config/icfn_config.yml")[::Rails.env]
  ICFN_ID = CREDENTIALS_CONFIG['icfn_id'].to_i
  ICFN_AUTHENTICATION_TOKEN = CREDENTIALS_CONFIG['icfn_authentication_token']

  class << self

    # => Accessors ...
    attr_accessor :icfn_forwarding_scheduler, :data_sync_scheduler # => This needs to be moved from here later. Not clean...


    # => Methods ...
    def initiate_icfn_services

      return if IcfnMain.icfn_forwarding_scheduler.present?

      
      IcfnMain.icfn_forwarding_scheduler = Rufus::Scheduler.new

      # => use a hash store to store the configuration in the fog_maintain repo ...


      IcfnMain.icfn_forwarding_scheduler.every '10s' do
        # => Need to be changed the timer into one hour ...

        InfraParameter.all.each do |infra_parameter|

          IcfnMain.update_house_ids_cache
          cached_house_ids = House.cached_house_ids

          infra_parameter_id = infra_parameter.id
          retrieve_type = infra_parameter.retrieve_type


          if cached_house_ids.present? && cached_house_ids.is_a?(Array)

            cached_house_ids.each do | house_id |

              computed_measurement = nil

              measurements = []

              $redis.keys("cached_icfn_entry_#{house_id}_#{infra_parameter_id}_timestamp_*").compact.each do |cached_measurement|
                measurements << $redis.get(cached_measurement).to_f
              end

              if retrieve_type == InfraParameter::INFRA_PARAMETER_RETRIEVE_TYPE_SUM
                computed_measurement = measurements.sum if measurements.any?
              elsif retrieve_type == InfraParameter::INFRA_PARAMETER_RETRIEVE_TYPE_AVERAGE
                computed_measurement = measurements.average if measurements.any?
              elsif retrieve_type == InfraParameter::INFRA_PARAMETER_RETRIEVE_TYPE_LAST_VALUE
                computed_measurement = measurements.last if measurements.any?
              end

              $icfn_logger.info " #{retrieve_type}, #{house_id}, #{infra_parameter_id}"
              $icfn_logger.info " computed_measurement -> #{computed_measurement}"

              if computed_measurement.present?
                FogNetworkManager.send_reading_info_to_cloud( house_id, infra_parameter_id, computed_measurement )
              end
            end
          end
        end
      end

      IcfnMain.synchronize_data_periodically
      Rails.logger.info "\n Initiated Updated Management - Fog Layer 3"    
    end


    def synchronize_data_periodically
      # => Method which will check for data content based updates from the fog maintain server ..

      return if House.count < 1

      IcfnMain.data_sync_scheduler = Rufus::Scheduler.new if IcfnMain.data_sync_scheduler.nil?

      
      IcfnMain.data_sync_scheduler.every '30s' do
      
        update_status = FogNetworkManager.fetch_update_status
    
        if update_status == false 
          # => Need to look for a appraoch later so that i can store the fog_fetch_api and fog_post_api in a variable ...
          # => Currently can't do since it is leading to a delay job deserialization problem ...

          FogNetworkManager.synchronize_all_model_entities(nil, nil)
          FogNetworkManager.update_icfn_node_updation_status_in_fog_maintain
        end
    
      end

    end

    
    def update_house_ids_cache
      # => syncing house ids in cache by caching house ids stored in current database ...
      
      house_ids = House.all.map(&:id)
      House.cache_house_ids( house_ids )
    end

  end  
end