class FogSecurityManager

  # => Constants ...
  DEFAULT_SYMMETRIC_ENCRYPTION_SCHEME = "AES-192-CBC"
  KEY_SIZE_BYTES = 24


  class << self

    def random_encryption_key(key_size_bytes = nil)
      # => This function will generate a random encrytion key ...
      # => The encryption key will have a size of key_size_bytes bytes ...

      key_size_bytes ||= KEY_SIZE_BYTES

      encryption_key = Devise.friendly_token
      encryption_key_padded_keysize_bytes = encryption_key.ljust(KEY_SIZE_BYTES, '0')
      encryption_key_padded_keysize_bytes
    end

    def symmetrically_encrypted_payload_data(plain_payload_data_hash = {}, symmetric_encryption_scheme = nil)
      # => This function will symmetrically encrypt the payload data in hash format ...
      # => The hash is converted to json string ...
      # => The json string is encrypted and the Base64 encoded format is returned ...

      symmetric_encryption_scheme ||= DEFAULT_SYMMETRIC_ENCRYPTION_SCHEME
  
      cipher = OpenSSL::Cipher.new(symmetric_encryption_scheme)
      cipher.encrypt

      authentication_token = IcfnMain::CREDENTIALS_CONFIG['icfn_authentication_token']
      authentication_token_padded_24_bytes = authentication_token.ljust(KEY_SIZE_BYTES, '0')
      cipher.key = authentication_token_padded_24_bytes

      plain_payload_data = plain_payload_data_hash.to_json

      encrypted_payload_data = cipher.update(plain_payload_data) + cipher.final
      Base64.encode64(encrypted_payload_data)    
    end


    def retrieve_plain_json_from_encrypted_json_list(encrypted_json_list = nil)
      return if encrypted_json_list.nil?

      plain_json_list = []
      encrypted_json_list.each do |encrypted_json|
        plain_json_list << retrieve_plain_json_from_encrypted_json(encrypted_json)
      end

      plain_json_list
    end
   
    
    def retrieve_plain_json_from_encrypted_json(encrypted_json)
      # => The following function retrieves the plain hash based data from the json response ...
      # => The json response includes asymmetrically encrypted symmetric key information and symmetrically encrypted payload data  ..
      # => The symmetric key and initial vector are retrieved from the asymmetrically encrypted data ...
      # => The icfn information from yml file is used to decrypt the asymmetrically encrypted content as the private key as the house information ...
      # => The symmetric key information retrieved is used to decrypt the encrypted data payload ...
      # => The net result is the payload data ...

      asymmetrically_encrypted_symmetric_encryption_info = Base64.decode64(encrypted_json["asymmetrically_encrypted_symmetric_encryption_info"])
      symmetrically_encrypted_payload_data = Base64.decode64(encrypted_json["symmetrically_encrypted_payload_data"])
      
      private_key_content = File.read("icfn_node_#{IcfnMain::ICFN_ID}_private_key.pem")
      private_key = OpenSSL::PKey::RSA.new(private_key_content, nil)
      asymmetrically_encrypted_symmetric_encryption_info_json_string = private_key.private_decrypt(Base64.decode64(asymmetrically_encrypted_symmetric_encryption_info))

      asymmetrically_encrypted_symmetric_encryption_info_json = JSON.parse(asymmetrically_encrypted_symmetric_encryption_info_json_string)
      base64_encoded_symmetric_key = asymmetrically_encrypted_symmetric_encryption_info_json["symmetric_key"]
      base64_encoded_initial_vector = asymmetrically_encrypted_symmetric_encryption_info_json["initial_vector"]

      symmetric_encryption_scheme = asymmetrically_encrypted_symmetric_encryption_info_json["symmetric_encryption_scheme"]
      symmetric_key = Base64.decode64(base64_encoded_symmetric_key)
      initial_vector = Base64.decode64(base64_encoded_initial_vector)

      puts "\n encryption_info - #{symmetric_encryption_scheme}, #{symmetric_key}, #{initial_vector}"


      decipher = OpenSSL::Cipher.new(symmetric_encryption_scheme)
      decipher.decrypt
      decipher.key = symmetric_key
      decipher.iv = initial_vector

      payload_data_json_string = decipher.update(symmetrically_encrypted_payload_data) + decipher.final
      plain_json = JSON.parse(payload_data_json_string)
      plain_json
    end


    def symmetrically_encrypt_plain_data_hash(plain_data_hash = {}, encryption_key = nil, encryption_scheme = nil, opts = {})
      # => This function will symmetrically encrypt the payload data in hash format ...
      # => The encryption key and encryption scheme are taken as function arguments ...
      # => The hash is converted to json string ...
      # => The json string is encrypted and the Base64 encoded format is returned ...

      encryption_scheme ||= DEFAULT_SYMMETRIC_ENCRYPTION_SCHEME
  
      cipher = OpenSSL::Cipher.new(encryption_scheme)
      cipher.encrypt

      encryption_key_padded_keysize_bytes = encryption_key.ljust(KEY_SIZE_BYTES, '0')
      cipher.key = encryption_key_padded_keysize_bytes

      plain_payload_data = plain_data_hash.to_json

      encrypted_payload_data = cipher.update(plain_payload_data) + cipher.final
      Base64.encode64(encrypted_payload_data)    
    end



    def payload_data_hash_from_symmetrically_encrypted_payload_data(encrypted_payload, encryption_key = nil, encryption_scheme = nil, opts = {})
      # => The following function retrieves the plain hash based data from the json response ...
      # => The json response includes asymmetrically encrypted symmetric key information and symmetrically encrypted payload data  ..
      # => The symmetric key and initial vector are retrieved from the asymmetrically encrypted data ...
      # => The current house information is used to decrypt the asymmetrically encrypted content as the private key as the house information ...
      # => The symmetric key information retrieved is used to decrypt the encrypted data payload ...
      # => The net result is the payload data ...

      raise "Invalid encrypted payload data" if encrypted_payload.nil?
      raise "Invalid Encryption Key" if encryption_key.nil?

      base64_decoded_symmetrically_encrypted_payload = Base64.decode64(encrypted_payload)
      encryption_scheme ||= DEFAULT_SYMMETRIC_ENCRYPTION_SCHEME
      encryption_key_padded_keysize_bytes = encryption_key.ljust(KEY_SIZE_BYTES, '0')

      decipher = OpenSSL::Cipher.new(encryption_scheme)
      decipher.decrypt
      decipher.key = encryption_key_padded_keysize_bytes
    
      payload_data_json_string = decipher.update(base64_decoded_symmetrically_encrypted_payload) + decipher.final
      payload_data_hash = JSON.parse(payload_data_json_string)
      payload_data_hash
    end


    def icfn_to_cloud_asymmetrically_encrypted_hash(cloud_id, plain_data_hash)
      # => The following method is to encrypt asymmterically the data send from icfn to fog ...
      # => The cloud public key file is stored in the icfn node which is used for encryption ...
    
      public_key_content = File.read("cloud_#{cloud_id}_icfn_public_key.pem")
      public_key = OpenSSL::PKey::RSA.new(public_key_content)
      encrypted_string = Base64.encode64(public_key.public_encrypt(plain_data_hash.to_json))
      encrypted_string
    end


    def fog_request_encoded_token_signature
      # => The following method generates the base64 token signature ...
      # => The message signed contains house_id and house_authentication_token ...
  
      private_key_content = File.read("icfn_node_#{IcfnMain::ICFN_ID}_private_key.pem")
      private_key = OpenSSL::PKey::RSA.new(private_key_content, nil)
      plain_message = { icfn_id: IcfnMain::ICFN_ID, icfn_authentication_token: IcfnMain::ICFN_AUTHENTICATION_TOKEN }.to_json            
      signature = private_key.sign(OpenSSL::Digest::SHA256.new, plain_message)
      Base64.encode64(signature)
    end
  end
end