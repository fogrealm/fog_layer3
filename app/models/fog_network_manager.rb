class FogNetworkManager

  CREDENTIALS_CONFIG = YAML.load_file("#{::Rails.root}/config/icfn_config.yml")[::Rails.env]
  
  #CLOUD_BASE_URL = "http://ec2-13-233-93-161.ap-south-1.compute.amazonaws.com:5000"
  #CLOUD_BASE_URL = "http://0.0.0.0:5000"
  CLOUD_BASE_URL = CREDENTIALS_CONFIG['cloud_base_url']
  CLOUD_ID = CREDENTIALS_CONFIG['cloud_id']

  CLOUD_TEST_EVENT_ROUTE_PATH = "/house_events/test_event_controller.json"
  CLOUD_SEND_INFRA_READING_PATH = "/infra_readings.json"
  CLOUD_SEND_HOUSE_EVENT_PATH = "/house_events.json"

  #FOG_MAINTAIN_BASE_URL = "http://0.0.0.0:5050"
  FOG_MAINTAIN_BASE_URL = "http://ec2-13-232-107-4.ap-south-1.compute.amazonaws.com"


  class << self

    def send_reading_info_to_cloud(house_id, infra_parameter_id, measurement)
      fog_api = FogApi.new(CLOUD_BASE_URL)
        
      payload_data = {
        house_id: house_id,
        infra_parameter_id: infra_parameter_id,
        value: measurement
      }

      payload = {}
      payload[:infra_reading] = payload_data

      encryption_key = FogSecurityManager.random_encryption_key
      encrypted_payload = FogSecurityManager.symmetrically_encrypt_plain_data_hash(payload, encryption_key, nil, nil)
      asymmetrically_encrypted_symmetric_key_info = FogSecurityManager.icfn_to_cloud_asymmetrically_encrypted_hash(CLOUD_ID, {channel_encryption_key: encryption_key})

      message_digest = OpenSSL::HMAC.hexdigest("SHA256", encryption_key, payload.to_json)

      final_payload = {
        encrypted_payload: encrypted_payload,
        asymmetrically_encrypted_symmetric_key_info: asymmetrically_encrypted_symmetric_key_info,
        message_digest: message_digest
      }

      puts "\n sending final payload from icfn - #{final_payload}, encryption_key - #{encryption_key}"

      response = fog_api.conn.post CLOUD_SEND_INFRA_READING_PATH, final_payload.to_json

      $icfn_logger.info "\n\n payload to cloud from icfn - #{payload}"
    end


    def fetch_update_status
      # => This Performs an API call to fog maintain to check whether any update is required or not ...

      fog_api = FogApi.new(FOG_MAINTAIN_BASE_URL)
      check_update_route = "/icfn_nodes/1/check_updates.json"
      
      response = fog_api.conn.get check_update_route, { ignore_security: true }
      puts "response - #{response.body}"

      response_body = JSON.parse(response.body)
      updation_status = response_body["updation_status"]

      status  = updation_status["is_recently_updated"]

      status
      #puts "\n updation status - #{updation_status}"
      
    end


    def synchronize_all_model_entities(fog_fetch_api = nil, fog_post_api = nil)
      # => This method will update all the model entities within the fog layer 1 ...
      # => The Data is fetched from fog maintain server ...
      # => Once the data within the layer 1 is synchronized ...
      # => Then, the corresponding layer 2 entities (if only needed to ) will be also synchronized ...

      EndDeviceInfo.fetch_and_update_end_device_infos_from_fog_maintain(fog_fetch_api, fog_post_api)
      InfraParameter.fetch_and_update_infra_parameters_from_fog_maintain(fog_fetch_api, fog_post_api)
      EndDeviceInfoInfraParameter.fetch_and_update_end_device_info_infra_parameters_from_fog_maintain(fog_fetch_api, fog_post_api)
      House.fetch_and_update_houses_from_fog_maintain(fog_fetch_api, fog_post_api)
      ServiceInfra.fetch_and_update_service_infras_from_fog_maintain(fog_fetch_api, fog_post_api)
      HouseServiceInfra.fetch_and_update_house_service_infras_from_fog_maintain(fog_fetch_api, fog_post_api)
    end

    
    def update_icfn_node_updation_status_in_fog_maintain
      # => This method will update the status of the house as updated ...
    
      fog_updation_complete_api = FogApi.new(FogNetworkManager::FOG_MAINTAIN_BASE_URL)    
      set_icfn_node_update_route = "/icfn_nodes/1/set_icfn_node_recently_updated.json"

      fog_updation_complete_api.conn.post set_icfn_node_update_route, { ignore_security: true }.to_json
    end    

    

    handle_asynchronously :send_reading_info_to_cloud
  end

end