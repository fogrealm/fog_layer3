module Conditions::House

  def self.is_cached?( house_id )
    # => Condition to check the house id received is cached ...

    house_ids_json = $redis.get("cached_house_ids_json")
    house_ids = JSON.parse(house_ids_json)

    is_cached_status = true

    if house_ids == "" || house_ids == nil || house_ids.include?(house_id) == false
      is_cached_status = false
    end

    is_cached_status
  end

  def is_saved_in_db?( house_id )
    # => Condition to check whether the house id is saved in db ...

    House.where(House::HOUSE_AREL[:id].eq( house_id )).first.present?
  end




end