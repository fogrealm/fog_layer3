module Actions::IcfnMain
  
  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
  end


  module ClassMethods

  end
  
  module InstanceMethods
    
  end
  
  
end