module Actions::HouseServiceInfra
  
  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
  end
end


module ClassMethods

  def fetch_and_update_house_service_infras_from_fog_maintain( fog_fetch_api = nil, fog_post_api = nil )
    # => This method to fetch the current house from server and update this in the fog ...

    fog_fetch_api = FogApi.new(FogNetworkManager::FOG_MAINTAIN_BASE_URL) if fog_fetch_api.nil?

    fetch_house_service_infras_route = "/house_service_infras.json"

    encoded_token_signature = FogSecurityManager.fog_request_encoded_token_signature
    plain_payload_data_hash = {
      encoded_token_signature: encoded_token_signature,
      icfn_id: IcfnMain::ICFN_ID,
      icfn_authentication_token: IcfnMain::ICFN_AUTHENTICATION_TOKEN
    }
    encrypted_request_payload = FogSecurityManager.symmetrically_encrypted_payload_data(plain_payload_data_hash, nil)

    encrypted_json_list_response = fog_fetch_api.conn.get fetch_house_service_infras_route, { encrypted_request_payload: encrypted_request_payload, layer: 3, icfn_id: IcfnMain::ICFN_ID }
    puts "encrypted json list response - #{encrypted_json_list_response.body}"

    encrypted_json_list = JSON.parse(encrypted_json_list_response.body)
    house_service_infras_json = FogSecurityManager.retrieve_plain_json_from_encrypted_json_list(encrypted_json_list) rescue nil
    puts "\n plain response json - #{house_service_infras_json}"

    if house_service_infras_json.present?

      current_existing_ids = house_service_infras_json.map{|house_service_infra_json| house_service_infra_json["id"]}
      house_service_infras_to_be_removed = HouseServiceInfra.where.not(id: current_existing_ids)
      house_service_infras_to_be_removed.destroy_all

      house_service_infras_json.each do |house_service_infra_json|
        id = house_service_infra_json["id"]
        house_id = house_service_infra_json["house_id"]
        service_infra_id = house_service_infra_json["service_infra_id"]
        
        house_service_infra = HouseServiceInfra.where(id: id).first

        if house_service_infra.nil?
          house_service_infra = HouseServiceInfra.new
        end

        house_service_infra.id = id
        house_service_infra.house_id = house_id
        house_service_infra.service_infra_id = service_infra_id
        
        house_service_infra.save!
      end

    end
  end

  handle_asynchronously :fetch_and_update_house_service_infras_from_fog_maintain

end


module InstanceMethods

end