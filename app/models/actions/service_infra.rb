module Actions::ServiceInfra
  
  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
  end
end


module ClassMethods

  def fetch_and_update_service_infras_from_fog_maintain( fog_fetch_api = nil, fog_post_api = nil )
    # => This method to fetch the current house from server and update this in the fog ...

    fog_fetch_api = FogApi.new(FogNetworkManager::FOG_MAINTAIN_BASE_URL) if fog_fetch_api.nil?

    fetch_service_infras_route = "/service_infras.json"

    encoded_token_signature = FogSecurityManager.fog_request_encoded_token_signature
    plain_payload_data_hash = {
      encoded_token_signature: encoded_token_signature,
      icfn_id: IcfnMain::ICFN_ID,
      icfn_authentication_token: IcfnMain::ICFN_AUTHENTICATION_TOKEN
    }
    encrypted_request_payload = FogSecurityManager.symmetrically_encrypted_payload_data(plain_payload_data_hash, nil)

    encrypted_json_list_response = fog_fetch_api.conn.get fetch_service_infras_route, { encrypted_request_payload: encrypted_request_payload, layer: 3, icfn_id: IcfnMain::ICFN_ID }
    puts "encrypted json list response - #{encrypted_json_list_response.body}"

    encrypted_json_list = JSON.parse(encrypted_json_list_response.body)
    service_infras_json = FogSecurityManager.retrieve_plain_json_from_encrypted_json_list(encrypted_json_list) rescue nil

    puts "\n plain response json - #{service_infras_json}"

    if service_infras_json.present?

      current_existing_ids = service_infras_json.map{|service_infra_json| service_infra_json["id"]}
      service_infras_to_be_removed = ServiceInfra.where.not(id: current_existing_ids)
      service_infras_to_be_removed.destroy_all

      service_infras_json.each do |service_infra_json|
        id = service_infra_json["id"]
        name = service_infra_json["name"]
        priority = service_infra_json["priority"]
        
        service_infra = ServiceInfra.where(id: id).first

        if service_infra.nil?
          service_infra = ServiceInfra.new
        end

        service_infra.id = id
        service_infra.name = name
        service_infra.priority = priority
        
        service_infra.save!
      end

    end
  end

  handle_asynchronously :fetch_and_update_service_infras_from_fog_maintain

end


module InstanceMethods

end