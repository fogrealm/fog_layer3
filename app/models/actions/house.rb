module Actions::House
  
  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
  end


  module ClassMethods
    
    def cache_house_id( house_id )
      # => save the house id in cache ...

      house_ids_json = $redis.get("cached_house_ids_json")
      house_ids = JSON.parse(house_ids_json)

      new_house_ids_json = nil

      if house_ids_json.nil? || house_ids_json == ""
        new_house_ids_json = [ house_id ].to_json
      else
        house_ids_array = JSON.parse(house_ids_json)
        house_ids_array << house_id unless house_ids_array.include?(house_id)
        new_house_ids_json = house_ids_array.to_json
      end

      $redis.set("cached_house_ids_json", new_house_ids_json) if new_house_ids_json.present?

    end

    
    def cache_house_ids( house_ids )
      # => cache all the house ids in database to cache ...
      
      house_ids_json = $redis.get("cached_house_ids_json")
      new_house_ids_json = nil

      house_ids.each do |house_id|

        if house_ids_json.nil? || house_ids_json == ""
          new_house_ids_json = [ house_id ].to_json
        else
          house_ids_array = JSON.parse(house_ids_json)
          house_ids_array << house_id unless house_ids_array.include?(house_id)
          new_house_ids_json = house_ids_array.to_json
        end
        
        $redis.set("cached_house_ids_json", new_house_ids_json)
        house_ids_json = new_house_ids_json
      end

    end


    def fetch_and_update_houses_from_fog_maintain( fog_fetch_api = nil, fog_post_api = nil )
    # => This method to fetch the current house from server and update this in the fog ...

      fog_fetch_api = FogApi.new(FogNetworkManager::FOG_MAINTAIN_BASE_URL) if fog_fetch_api.nil?
      fog_post_api = FogApi.new(FogNetworkManager::CLOUD_BASE_URL) if fog_post_api.nil?

      fetch_houses_route = "/houses.json"
      
      encoded_token_signature = FogSecurityManager.fog_request_encoded_token_signature
      plain_payload_data_hash = {
        encoded_token_signature: encoded_token_signature,
        icfn_id: IcfnMain::ICFN_ID,
        icfn_authentication_token: IcfnMain::ICFN_AUTHENTICATION_TOKEN
      }
      encrypted_request_payload = FogSecurityManager.symmetrically_encrypted_payload_data(plain_payload_data_hash, nil)

      encrypted_json_list_response = fog_fetch_api.conn.get fetch_houses_route, { encrypted_request_payload: encrypted_request_payload, layer: 3, icfn_id: IcfnMain::ICFN_ID }
      puts "encrypted json list response - #{encrypted_json_list_response.body}"

      encrypted_json_list = JSON.parse(encrypted_json_list_response.body)
      houses_json = FogSecurityManager.retrieve_plain_json_from_encrypted_json_list(encrypted_json_list) rescue nil

      puts "\n plain response json - #{houses_json}"

      if houses_json.present?
        current_existing_ids = houses_json.map{|house_json| house_json["id"]}
        houses_to_be_removed = House.where.not(id: current_existing_ids)
        houses_to_be_removed.destroy_all

        houses_json.each do |house_json|

          id = house_json["id"]
          name = house_json["name"]
          address = house_json["address"]
          landmark = house_json["description"]

          location_id = house_json["location"]["id"]
          location_latitude = house_json["location"]["latitude"]
          location_longitude = house_json["location"]["longitude"]

          icfn_channel_key = nil
          nearby_icfn_node_json = house_json["nearby_icfn_node"]
          if nearby_icfn_node_json.present?
            icfn_channel_key = house_json["nearby_icfn_node"]["icfn_channel_key"]
          end

          house = House.where(id: id).first

          if house.present?
            house.id = id
            house.name = name
            house.address = address
            house.landmark = landmark

            house.location.id = location_id
            house.location.latitude = location_latitude
            house.location.longitude = location_longitude

            house.icfn_channel_key = icfn_channel_key

            house.save!
          else
            house = House.new
            house.id = id
            house.name = name
            house.address = address
            house.landmark = landmark

            location = house.build_location
            location.id = location_id
            location.latitude = location_latitude
            location.longitude = location_longitude

            house.icfn_channel_key = icfn_channel_key

            house.save!
          end
    
        end
    
        inform_cloud_data_route = "/houses/sync_all_houses.json"
        fog_post_api.conn.post inform_cloud_data_route
      end
    end

    handle_asynchronously :fetch_and_update_houses_from_fog_maintain

  end
  
  module InstanceMethods
    
  end
  
  
end