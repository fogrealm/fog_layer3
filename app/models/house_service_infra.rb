class HouseServiceInfra < ApplicationRecord
  include Actions::HouseServiceInfra
  
  # => Relations ...
  belongs_to :house
  belongs_to :service_infra

  
  # => Validations ...
  validates :house, :service_infra, presence: true

end
