class ServiceInfra < ApplicationRecord
  include Actions::ServiceInfra
 
  # => Constants ...
  MIN_PRIORITY = 1
  MAX_PRIORITY = 5

  SERVICE_INFRA_FIRE_ALARM_SYSTEM_TEMPERATURE_THRESHOLD = 40.56
  SERVICE_INFRA_FIRE_ALARM_SYSTEM_PHOTOSENSITIVE_VOLTAGE_THRESHOLD = 0.9
  SERVICE_INFRA_FIRE_ALARM_SYSTEM_ION_VOLTAGE = 0.15
  SERVICE_INFRA_FIRE_ALARM_SYSTEM_CO_THRESHOLD = 17
  SERVICE_INFRA_FIRE_ALARM_SYSTEM_CO2_THRESHOLD = 22

  SERVICE_INFRA_SCENARIO_NORMAL_ALL = "scenario_normal_all"
  SERVICE_INFRA_SCENARIO_NORMAL_ALARM_SYSTEM = "scenario_normal_alarm_system"
  SERVICE_INFRA_SCENARIO_FIRE_OCCURS = "scenario_fire_occurs"

  SCENARIO_FIRE_TEMPERATURE_INCREASE_PER_SECOND = 0.25
  SCENARIO_FIRE_CO_INCREASE_PER_SECOND = 1.00
  SCENARIO_FIRE_CO2_INCREASE_PER_SECOND = 10.00
  SCENARIO_FIRE_MOX_GS822_INCREASE_PER_SECOND = 0.01
  SCENARIO_FIRE_MOX_TGS880_INCREASE_PER_SECOND = 0.001
  SCENARIO_FIRE_SMOKE_VOLTAGE_INCREASE_PER_SECOND = 0.001
  SCENARIO_FIRE_ION_VOLTAGE_INCREASE_PER_SECOND = 0.01

  SERVICE_INFRA_SCENARIOS = [
    SERVICE_INFRA_SCENARIO_NORMAL_ALL,
    SERVICE_INFRA_SCENARIO_NORMAL_ALARM_SYSTEM,
    SERVICE_INFRA_SCENARIO_FIRE_OCCURS
  ]

  SERVICE_INFRA_ROUTE_FIRE_ALARM_CONTROLLER = "/house_events/fire_alarm_controller.json"
  SERVICE_INFRA_ROUTE_SMART_METER_CONTROLLER = "/house_events/smart_meter_controller.json"


  # => Relations ...
  has_many :house_service_infras, :dependent => :destroy
  has_many :houses, :through => :house_service_infras
  
  
  # => Validations ...
  validates :name, presence: true
  validates :priority, presence: true, inclusion: { :in => MIN_PRIORITY .. MAX_PRIORITY }

  
end