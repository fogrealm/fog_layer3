class HouseEvent < ApplicationRecord
  
  # => Constants ...
  HOUSE_EVENT_TYPE_ALERT = "alert"
  HOUSE_EVENT_TYPE_INFORM = "inform"

  HOUSE_EVENT_TYPES = [
    HOUSE_EVENT_TYPE_ALERT,
    HOUSE_EVENT_TYPE_INFORM
  ]
  

  # => Relations ...
  belongs_to :house
  

  # => Validations ...
  validates :name, presence: true
  validates :house_section_name, presence: true
  validates :house_event_type, inclusion: { :in => HOUSE_EVENT_TYPES }
  validates :event_level, inclusion: { :in => (1..10)}
  validates :reported_at, presence: true


  # => Callbacks ...
  before_validation on: :create do 
    # => If house event type is not mentioned, then we take type as 'inform' by default ...
    # => If event level is not mentioned, then we take default level as 1 ...

    if self.house_event_type.nil?
      self.house_event_type = HOUSE_EVENT_TYPE_INFORM
    end

    if self.event_level.nil?
      self.event_level = 1
    end

    if self.reported_at_timestamp.present?
      self.reported_at = Time.at(self.reported_at_timestamp)
    end

  end


  after_save :inform_neighbours_if_house_event_type_alert


  # => Accessors ...
  attr_accessor :reported_at_timestamp

  private

  def inform_neighbours_if_house_event_type_alert
  	# => This function will inform the neighbours if the current house event is of alert type ...

    return if self.house_event_type != HOUSE_EVENT_TYPE_ALERT

    neighbour_admin_users = AdminUser.where.not(house_id: self.house_id)
    neighbour_phone_numbers = neighbour_admin_users.map(&:phone_number)

    admin_users_info = AdminUser.where(house_id: self.house_id).map{|admin_user| "#{admin_user.name} - #{admin_user.phone_number}"}.join(", ")
  	
    message = "Alert your neighbour. " + self.event_message + ". Inform #{admin_users_info}"
    $icfn_logger.info "Informing neighbours #{neighbour_phone_numbers} about the fire alert happened at house id #{self.house_id} ..."

    SmsManager.send_sms(to: neighbour_phone_numbers, message: message)

  end

end
