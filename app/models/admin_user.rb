class AdminUser < ApplicationRecord

  include Formats::AdminUser

  # => Relations ...
  belongs_to :house

  # => Validations ...
  validates :name, presence: true
  validates :phone_number, presence: true, format: { with: ActiveRecordBaseCommon::Validations::VALID_PHONE_FORMAT }
  
  validates_uniqueness_of :phone_number

end
