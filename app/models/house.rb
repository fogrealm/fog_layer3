class House < ApplicationRecord
  include ActiveRecordBaseCommon::Validations
  include Formats::House
  include Actions::House
  include Attributes::House
  include Conditions::House

  # => Constants ...
  HOUSE_AREL = House.arel_table

  # => Relations ...
  has_many :house_service_infras, :dependent => :destroy
  has_many :service_infras, :through => :house_service_infras
  
  has_many :admin_users, :dependent => :destroy
  
  has_one :location, as: :locatable, :dependent => :destroy
  

  # => Validations ...
  validates :name, :address, :landmark, presence: true
  validates :location, has_one: true
  

  # => Associations ...
  accepts_nested_attributes_for :location
  

end
