module Attributes::House

  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
  end


  module ClassMethods
    
    def cached_house_ids
      # => obtaining the cached house ids ...

      house_ids_json = $redis.get("cached_house_ids_json")
      
      res = nil
      if house_ids_json.present? && house_ids_json != ""
        res = JSON.parse(house_ids_json) rescue res = nil
      end

      res
    end
  end
  
  module InstanceMethods
    
  end
 
end