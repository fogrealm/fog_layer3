# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
if Rails.env.development?
  
  house = House.new
  house.id = 2
  house.name = "Wolf House"
  house.address = "Celtic Ville, Wolfstreet"
  house.landmark = "Near Wolfenstein Castle"
  house.description = "2 km from Dragon Bus Terminal"

  location = house.build_location
  location.latitude, location.longitude = 10.7606682, 78.8147379

  house.icfn_channel_key = "TyXJKxsNusxYyukA4csz"

  house.save!

  admin_user = AdminUser.new
  admin_user.id = 2
  admin_user.email = "anjalitp@gmail.com"
  admin_user.name = "Anjali T P"
  admin_user.phone_number = "9995516145"
  admin_user.house_id = 2
  admin_user.save!


  house = House.new
  house.id = 1
  house.name = "Dragon House"
  house.address = "Dragon Ville, Dragon Street"
  house.landmark = "Near Pheonix House"
  house.description = "2 km from Dragon Bus Terminal"

  location = house.build_location
  location.latitude, location.longitude = 10.7598267, 78.81599
  
  house.icfn_channel_key = "c2mC_zpvjcYphBCrEay2"

  house.save!

  admin_user = AdminUser.new
  admin_user.id = 1
  admin_user.email = "arjunthedragon@gmail.com"
  admin_user.name = "Arjun K P"
  admin_user.phone_number = "9072263675"
  admin_user.house_id = 1
  admin_user.save!


  EndDeviceInfo.create(
    [
      {
        "id"=>1, 
        "name"=>"CO Sensor", 
        "description"=>"It reads the CO Level in PPM", 
        "model"=>"008809809", 
        "manufacturer"=>"Cyber Dragon" 
      }, 
      {
        "id"=>2, 
        "name"=>"CO2 Sensor", 
        "description"=>"It reads the CO2 Level in PPM", 
        "model"=>"0088099000", 
        "manufacturer"=>"Cyber Dragon" 
      }, 
      {
        "id"=>3, 
        "name"=>"MOX GS822 Voltage Sensor", 
        "description"=>"Gas Sensor for Fire Detection", 
        "model"=>"GS822", 
        "manufacturer"=>"Woldra" 
      }, 
      {
        "id"=>4, 
        "name"=>"Temperature Sensor", 
        "description"=>"To read Temerature in Celcius", 
        "model"=>"CEL8777897", 
        "manufacturer"=>"Cyber Dragon" 
      }, 
      {
        "id"=>5, 
        "name"=>"MOX TGS880 Sensor", 
        "description"=>"Gas Sensor for Fire Detection", 
        "model"=>"89789797798", 
        "manufacturer"=>"Woldra" 
      }, 
      {
        "id"=>6, 
        "name"=>"Ion Voltage Sensor", 
        "description"=>"Sensor to read Ion votage in Gas", 
        "model"=>"ION8088909", 
        "manufacturer"=>"Woldra"
      }, 
      {
        "id"=>7, 
        "name"=>"Smart Meter Sensor", 
        "description"=>"Device to Read Electric Consumption in kwh.", 
        "model"=>"SMT900889", 
        "manufacturer"=>"Cyber Dragon" 
      }, 
      {
        "id"=>8, 
        "name"=>"Smoke Voltage Sensor", 
        "description"=>"Reads Smoke Voltage in the Gas", 
        "model"=>"098977888798", 
        "manufacturer"=>"Woldra"
      }
    ]
  )

  
  ServiceInfra.create(
    [
      {
        "id"=>1, 
        "name"=>"Fire Alarm Infrastructure", 
        "priority"=>1
      }, 
      {
        "id"=>2, 
        "name"=>"Smart Meter Infrastructure", 
        "priority"=>5
      }
    ]
  )


  InfraParameter.create(
    [
      {
        "id"=>2, 
        "name"=>"CO", 
        "unit"=>"ppm",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>3, 
        "name"=>"CO2", 
        "unit"=>"ppm",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>4, 
        "name"=>"MOX GS822", 
        "unit"=>"V",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>1, 
        "name"=>"Temperature", 
        "unit"=>"celcius",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>5, 
        "name"=>"MOX TGS880", 
        "unit"=>"V",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>6, 
        "name"=>"Smoke Voltage", 
        "unit"=>"V",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>7, 
        "name"=>"Ion Voltage", 
        "unit"=>"V",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>8, 
        "name"=>"Electric Consumption", 
        "unit"=>"kwh",
        "retrieve_type"=>"last_value"
      }
    ]
  )


  EndDeviceInfoInfraParameter.create(
    [
      {
        "id"=>1, 
        "end_device_info_id"=>1, 
        "infra_parameter_id"=>2
      }, 
      {
        "id"=>2, 
        "end_device_info_id"=>2, 
        "infra_parameter_id"=>3
      }, 
      {
        "id"=>3, 
        "end_device_info_id"=>3, 
        "infra_parameter_id"=>4
      }, 
      {
        "id"=>4, 
        "end_device_info_id"=>4, 
        "infra_parameter_id"=>1
      }, 
      {
        "id"=>5, 
        "end_device_info_id"=>5, 
        "infra_parameter_id"=>5
      }, 
      {
        "id"=>6, 
        "end_device_info_id"=>6, 
        "infra_parameter_id"=>7
      }, 
      {
        "id"=>7, 
        "end_device_info_id"=>7, 
        "infra_parameter_id"=>8
      }, 
      {
        "id"=>8, 
        "end_device_info_id"=>8, 
        "infra_parameter_id"=>6
      }
    ] 
  )

  HouseServiceInfra.create(
    [
      {
        "id" => 1,
        "house_id" => 1,
        "service_infra_id"=>1
      },
      {
        "id" => 2,
        "house_id" => 1,
        "service_infra_id"=>2
      },
      {
        "id" => 3,
        "house_id" => 2,
        "service_infra_id"=>1
      },
      {
        "id" => 4,
        "house_id" => 2,
        "service_infra_id"=>2
      }
    ]
  )
end