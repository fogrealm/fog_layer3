class CreateHouseServiceInfras < ActiveRecord::Migration[5.0]
  def change
    create_table :house_service_infras do |t|
      t.references :house, index: true
      t.references :service_infra, index: true

      t.timestamps
    end
  end
end
