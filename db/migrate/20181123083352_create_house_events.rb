class CreateHouseEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :house_events do |t|
     	
      t.string :name
      t.string :house_event_type, default: HouseEvent::HOUSE_EVENT_TYPE_INFORM
      t.integer :event_level, default: 1

      t.datetime :reported_at

      t.references :house, index: true
      t.string :house_section_name

      t.boolean :has_neighbours_informed, default: false, index: true
      
      t.timestamps
    end
  end
end