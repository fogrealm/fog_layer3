class AddUniqueIdentifierToAdminUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :admin_users, :unique_identifier, :string, unique: true
  end
end
