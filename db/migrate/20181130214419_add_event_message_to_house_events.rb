class AddEventMessageToHouseEvents < ActiveRecord::Migration[5.0]
  def change
    add_column :house_events, :event_message, :string
  end
end
