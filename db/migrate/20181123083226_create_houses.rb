class CreateHouses < ActiveRecord::Migration[5.0]
  def change
    create_table :houses do |t|
      t.string :name
      t.text :address
      t.string :landmark
      t.text :description

      t.string :icfn_channel_key

      t.timestamps
    end
  end
end
