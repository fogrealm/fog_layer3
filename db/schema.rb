# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181205065757) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admin_users", force: :cascade do |t|
    t.integer  "house_id"
    t.string   "email",             default: "", null: false
    t.string   "name",              default: "", null: false
    t.string   "phone_number",                   null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "unique_identifier"
    t.index ["house_id"], name: "index_admin_users_on_house_id", using: :btree
  end

  create_table "attachments", force: :cascade do |t|
    t.string   "attachable_type"
    t.integer  "attachable_id"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["attachable_type", "attachable_id"], name: "index_attachments_on_attachable_type_and_attachable_id", using: :btree
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree
  end

  create_table "end_device_info_infra_parameters", force: :cascade do |t|
    t.integer  "end_device_info_id"
    t.integer  "infra_parameter_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["end_device_info_id"], name: "index_end_device_info_infra_parameters_on_end_device_info_id", using: :btree
    t.index ["infra_parameter_id"], name: "index_end_device_info_infra_parameters_on_infra_parameter_id", using: :btree
  end

  create_table "end_device_infos", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "model"
    t.string   "manufacturer"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "house_events", force: :cascade do |t|
    t.string   "name"
    t.string   "house_event_type",        default: "inform"
    t.integer  "event_level",             default: 1
    t.datetime "reported_at"
    t.integer  "house_id"
    t.string   "house_section_name"
    t.boolean  "has_neighbours_informed", default: false
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.string   "event_message"
    t.index ["has_neighbours_informed"], name: "index_house_events_on_has_neighbours_informed", using: :btree
    t.index ["house_id"], name: "index_house_events_on_house_id", using: :btree
  end

  create_table "house_service_infras", force: :cascade do |t|
    t.integer  "house_id"
    t.integer  "service_infra_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["house_id"], name: "index_house_service_infras_on_house_id", using: :btree
    t.index ["service_infra_id"], name: "index_house_service_infras_on_service_infra_id", using: :btree
  end

  create_table "houses", force: :cascade do |t|
    t.string   "name"
    t.text     "address"
    t.string   "landmark"
    t.text     "description"
    t.string   "icfn_channel_key"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "infra_parameters", force: :cascade do |t|
    t.string   "name"
    t.string   "unit"
    t.string   "retrieve_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["name", "unit"], name: "index_infra_parameters_on_name_and_unit", unique: true, using: :btree
    t.index ["retrieve_type"], name: "index_infra_parameters_on_retrieve_type", using: :btree
  end

  create_table "locations", force: :cascade do |t|
    t.string   "locatable_type"
    t.integer  "locatable_id"
    t.float    "latitude"
    t.float    "longitude"
    t.float    "accuracy"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "service_infras", force: :cascade do |t|
    t.string   "name"
    t.integer  "priority"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_service_infras_on_name", using: :btree
    t.index ["priority"], name: "index_service_infras_on_priority", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.boolean  "is_suspended",           default: false
    t.string   "phone_number",                           null: false
    t.string   "authentication_token",                   null: false
    t.integer  "profile_id"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.index ["authentication_token"], name: "index_users_on_authentication_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["phone_number"], name: "index_users_on_phone_number", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

end
